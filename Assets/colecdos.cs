using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colecdos : MonoBehaviour
{
    public GameObject Efecto;


    private void Start()
    {
        Efecto.SetActive(false);
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
            Efecto.SetActive(true);

        }
    }
}
