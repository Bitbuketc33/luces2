using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class reiniciar : MonoBehaviour
{

    public GameObject Reiniciar;


    // Start is called before the first frame update
    void Start()
    {
        Reiniciar.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Respawn"))
        {
      
            Destroy(gameObject);
            Reiniciar.gameObject.SetActive(true);
        }
    }


}
